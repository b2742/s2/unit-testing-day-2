//Create a factorial function
const factorial = (n) => {
	if (n < 0) return undefined;
	if (typeof(n) !== 'number') return undefined;
	if(n === 0) return 1;
	if(n === 1) return 1;
	return n * factorial(n-1);
};

const div_check = (n) => {
	if(n % 5 === 0) return true
	if(n % 7 === 0) return true
	return false
};

const names = {
	Eren : {
		"alias" : "Attack Titan",
		"name" : "Eren Yeager",
		"age": 28
	},
	Levi : {
		"alias" : "Clean Boi",
		"name" : "Levi Ackerman",
		"age" : 38
	}
};

module.exports = {
	factorial : factorial,
	div_check : div_check,
	names : names
}