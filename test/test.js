const {factorial, div_check} = require('../src/util.js');
const {expect, assert} = require('chai');

describe('Test Factorials', () => {
	it('Test that 5! is 120', () => {
		const product = factorial(5)
		expect(product).to.equal(120)
		// assert.equal(product, 120)
	});

	it('Test that 1! is 1', () => {
		const product = factorial(1)
		assert.equal(product, 1)
	});

	it('Test that 0! is 1', () => {
		const product = factorial(0)
		expect(product).to.equal(1)
	});

	it('Test that 4! is 24', () => {
		const product = factorial(4)
		assert.equal(product, 24)
	});

	it('Test that 10! is 3628800', () => {
		const product = factorial(10)
		expect(product).to.equal(3628800)
	});

	it('Test negative factorial is undefined', () => {
		const product = factorial(-1)
		expect(product).to.equal(undefined)
	});

	it('Test if a non-numeric input returns an error', () => {
		const product = factorial('5')
		expect(product).to.equal(undefined)
	})
});

describe('Test divisibility by 5 and 7', () => {
	it('Test that 105 is divisible by 5', () => {
		const number = div_check(105)
		expect(number).to.equal('divisible by 5');
	});

	it('Test that 14 is divisible by 7', () => {
		const number = div_check(14)
		expect(number).to.equal('divisible by 7');
	});

	it('Test that 0 is divisible by both 5 or 7', () => {
		const number = div_check(0)
		expect(number).to.equal('divisible by 5'||'divisible by 7');
	});

	it('Test that 22 is not divisible by 5 or 7', () => {
		const number = div_check(22)
		expect(number).to.equal('not divisible by 5 or 7');
	});

});