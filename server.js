const express = require('express');
const app = express();

app.use(express.json())

require('./app/routes')(app, {});

app.listen(process.env.PORT || 5001, () => {
	console.log(`Running on port ${process.env.PORT || 5001}`)
})